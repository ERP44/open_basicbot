'''
Created on 28 ene. 2020
@author: rene.silva.developer
@summary:
Fecha: 
Esta clase debe cubrir las utilidades básicas de FECHAS

MUST:ESTA CLASE DEBE CONTAR AL MENOS CON:

1.-DATE FUNCTIONS: VALIDACIONES, RANGOS, DIFERENCIAS ENTRE FECHAS

2.-DATE FORMATTER: DEBE TRANSFORMAR UN DATO DE UN TIPO DE FORMATO A OTRO, EJEMPLOS: FECHAS

LIBRERIAS, DOCUMENTOS, TUTORIALES UTILIZADOS:
https://codigofacilito.com/articulos/fechas-python

'''
import logging
from datetime import datetime
from datetime import timedelta

class Fecha:

    __name_class = "Fecha"
    datetime_ = None

    def __init__(self):
        logging.info("FLUJO EN Fecha.__init__")
        self.datetime_ = datetime.now()

#    def __init__(self,fechaYYYYMMDD):
#        logging.info("FLUJO EN Fecha.__init__(fechaYYYYMMDD)")
        
        
    def formato(self):
        months = ("Enero", "Febrero", "Marzo", "Abri", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
        day = self.datetime_.day
        month = months[self.datetime_.month - 1]
        year = self.datetime_.year
        messsage = "{} de {} del {}".format(day, month, year)
        return messsage


    def formatoYYYYMMDD(self):
        salida = self.datetime_.year * 10000 + self.datetime_.month*100 + self.datetime_.day
        return salida

    def agregarDias(self,numero):
        self.datetime_ = self.datetime_ + timedelta(days=numero)
    
if __name__ == '__main__':
    
    
    fecha = Fecha()

    print ("------------------------------------------------------------------------")
    print (fecha)
    print (fecha.formato())
    print (fecha.formatoYYYYMMDD())
    fecha.agregarDias(1)
    
    print (fecha.formatoYYYYMMDD())
    
    
    
    print ("------------------------------------------------------------------------")

   
    pass

"""



#Sumar dos días a la fecha actual
now = datetime.now()
new_date = now + timedelta(days=2)
print(new_date)

 self.datetime_ 


    now = datetime.now()
    print(current_date_format(now))
    
    

"""

