'''
Created on 27 ene. 2020
Clase de prueba de BasicBot_TextMining
@author: rene.silva.developer
'''

from bots.BasicBot_TextMining_CLASS import BasicBot_TextMining 

def TEST_generaXML_FILA_CARROT2_DICT():
    test = BasicBot_TextMining("")
    dictDocumento = {"id":"1619792", "title":"PNO", "url":"http://www.google.cl","snippet": "Estacionamiento,siempre lleno el subterráneo y cero fiscalización donde se estacionan (tapan red de incendios y en los pasillos también estacionan), los baños el de primer piso pasado a pintura,mala ventilación,no hay cubre inodoro,SÓLO UN dispensador de jabón,espejos manchados "}
    xml_from_dict = test.generaXML_FILA_CARROT2_DICT (dictDocumento)
    return xml_from_dict

def TEST_generaXML_FROM_SQL():
    srcQuery = "select id, mall as title, 'http://www.google.cl' as url, CONTENT as snippet from [MALLPLAZA_MODEL].[dbo].[PNO_201710_TO_201910_RESUMIDO]"
    test = BasicBot_TextMining(srcQuery)
    xml_from_sql = ""
    #test.generaXML_FILA_CARROT2_DICT (dictDocumento)
    return xml_from_sql

def TEST_generaXML_FROM_FILE_CSV():
    srcFile = ""
    test = BasicBot_TextMining(srcFile)
    xml_from_sql = ""
    #test.generaXML_FILA_CARROT2_DICT (dictDocumento)
    return xml_from_sql

if __name__ == '__main__':

    #GENERA FILA DOCUMENTO CARROT A PARTIR DE UN DICCIONARIO
    #SIRVE PARA ANALIZAR SOLO UN DOCUMENTO

    print( "-------------------------------------" )
    print( "----PRUEBAS BasicBot_TextMining------" )
    print( "----@author: rene.silva.developer----" )
    print( "-------------------------------------\n" )
    
    salidaXML = TEST_generaXML_FILA_CARROT2_DICT()

    print( "TEST_generaXML_FILA_CARROT2_DICT-->salidaXML" )
    print( "-----------------------------------" )
    print( str(salidaXML) )
    print( "-----------------------------------\n" )

    salidaXML = TEST_generaXML_FROM_SQL()

    print( "TEST_generaXML_FROM_SQL-->salidaXML" )
    print( "-----------------------------------" )
    print( str(salidaXML) )
    print( "-----------------------------------\n" )

    salidaXML = TEST_generaXML_FROM_FILE_CSV()

    print( "TEST_generaXML_FROM_FILE_CSV-->salidaXML" )
    print( "-----------------------------------" )
    print( str(salidaXML) )
    print( "-----------------------------------\n" )

    array = [
        {"id":"1", "title":"EL TITULO", "url":"LA URL","snippet": "EL TEXTO LIBRE"},
        {"id":"2", "title":"EL TITULO", "url":"LA URL","snippet": "EL TEXTO LIBRE"},
        {"id":"3", "title":"EL TITULO", "url":"LA URL","snippet": "EL TEXTO LIBRE"}
    ]
   
    pass

