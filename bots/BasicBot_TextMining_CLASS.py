'''
Created on 24 ene. 2020
@author: rene.silva.developer
@summary:
BasicBot_TextMining: 
Esta clase debe cubrir las funcionalidades básicas de análisis de texto EN ESPAÑOL

MUST:ESTA CLASE DEBE CONTAR AL MENOS CON

1.-CONECTARSE A FUENTES DE DATOS: ARCHIVOS PLANOS TXT, ARCHIVOS CSV Y MOTORES DE BASES DE DATOS

2.-CLEANSING - LIMPIAR Y DEPURAR TEXTO PARA SU PROXIMO ANALISIS
    DEBE NORMALIZAR EL TEXTO
        Debe eliminar caracteres que no sirven o no pertenecen al alfabeto
    DEBE ELIMINAR TEXTO QUE NO SIRVE
        Debe quitar palabras que no aportan valor al análisis del texto
    DEBE REEMPLAZAR ERRORES COMUNES DEL TEXTO EN ESPAÑOL
        Debe ser capaza de reconocer errores de las personas y traducir cuando sea posible
        
3.-GENERAR VECTORES DE PALABRAS, DE ORACIONES Y DE IDEAS
    DEBE GENERAR LAS METRICAS ESTANDAR PARA ANALISIS DE DOCUMENTOS
        
4.-GENERAR WORDCLOUDs
    DEBE GENERAR IMAGEN PNG DE LAS PALABRAS QUE CONTIENE EL TEXTO QUE SE ESTA ANALIZANDO

5.-TRANSFORMAR LA DATA PARA SER PROCESADA EN OTRAS HERRAMIENTAS OPEN SOURCE DE TEXMINING
    DEBE GENERAR XML DE DOCUMENTOS PARA CARROT2
        A partir de texto libre, genera código XML que será usado posteriormente por el clusterizador CARROT2


LIBRERIAS UTILIZADAS:

https://pypi.org/project/dicttoxml/


'''
import logging
from dicttoxml import dicttoxml
#from xml.dom.minidom import parseString

class BasicBot_TextMining:

    __name_class = "BasicBot_TextMining"
    
    __src_FOOD = "" #nombre del documento, ejemplo: ruta de un archivo CSV, UN QUERY UN EXCEL
    __src_TYPE = "" 

    __separador = "\t"
    __separador_fila = "\n"
    
    __http_txt = ""

    #salidas

    __data = None
    __salidaCSV = ""

    def __init__(self,__src_FOOD_):
        #se hace cargo de detectar el tipo de fuente
        self.__src_FOOD = __src_FOOD_

        if (__src_FOOD_==""):
            logging.info("ES VACIO")

        if ('select' in __src_FOOD_.lower()):
            logging.info("ES UN QUERY")

        if (__src_FOOD_.lower().endswith('csv')):
            logging.info("ES UN CSV")
            
        if (__src_FOOD_.lower().endswith('txt')):
            logging.info("ES UN TXT SEPARADO POR TABULAR")
        
    def generaXML_FILA_CARROT2_DICT(self, dict_data ):
        xml_fila = '<document id="' + str(dict_data.get('id')) + '">'
        #se hace un dict as partir del dict base, dado que el ID del documento debe ir en el TAG Document, y los tags:title,url,snippet debe ir dentro 
        #se debe usar una función XML, para que sean trandformados los caracteres
        nuevoDict ={"title":str(dict_data.get('title')), "url":str(dict_data.get('url')),"snippet": str(dict_data.get('snippet'))} 
        xml_snippet = dicttoxml(nuevoDict, root=False,attr_type=False)
        #dom = parseString(xml_snippet)
        xml_fila = xml_fila + xml_snippet.decode('utf-8') 
        xml_fila = xml_fila + '</document>' 
        return xml_fila

    def generaXML(self, lista_array, separadorFila ):
        
        xml_ = '<?xml version="1.0" encoding="UTF-8"?>' + separadorFila
    
        xml_ = xml_ + '<searchresult>' + separadorFila
        xml_ = xml_ + '<query>PNO</query>' + separadorFila
    
    
        for fila in lista_array:
            #print("fila:" + str(fila) )
            xml_snippet = dicttoxml(fila, root=False)
            
            #print("xml_snippet:" + str(xml_snippet) )
            #xml_ = xml_ + str(xml_snippet)
            xml_fila = ""
            xml_fila = '<document id="122">' + separadorFila
            
            #dom = parseString(xml_snippet)
            
            xml_fila = xml_fila + xml_snippet.decode('utf-8') + separadorFila
            
            xml_fila = xml_fila + '</document>' + separadorFila
    
            #print(dom.toprettyxml())
            
            xml_ = xml_ + xml_fila + separadorFila
    
    #>>> dom = parseString(xml)
    #>>> print(dom.toprettyxml())
    
    #>>> xml_snippet = dicttoxml.dicttoxml(obj, root=False)
    #>>> print(xml_snippet)
    
    #    xml = dicttoxml(array, custom_root='searchresult', attr_type=False)
        
        xml_ = xml_ + '</searchresult>' + separadorFila
    
        return xml_


"""
<?xml version="1.0" encoding="UTF-8"?>
  <searchresult>
    <query>#or2012</query>

Row template
    <document id="{{row.index}}">
      <title>{{cells["text"].value}}</title>
      <url>{{cells["status_url"].value}}</url>
      <snippet>
        {{cells["text"].value}}
      </snippet>
    </document>

Row separator
[single carriage return]

Suffix
</searchresult>

"""


if __name__ == '__main__':
    
    
    a = BasicBot_TextMining("c:/a.txt")




    #salida = generaXML(None, '\n')
    
    #print ("------------------------------------------------------------------------")
    #print (salida)
    
    #print ("------------------------------------------------------------------------")




    
    pass






